(function($){
	'use strict';

	$(document).ready(function(){
		$.getJSON('data.json', function(data) {

			var template =  $('#template').html();
			var output = Mustache.render(template, data);
			$('#t4 .container').append(output);  

		});
	});

})(jQuery);