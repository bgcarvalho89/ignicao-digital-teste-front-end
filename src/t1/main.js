(function($){
	'use strict'

	$(document).ready(function() {

		var screenEl = $('.calc-area');
		var result;
		var operations = ['/','*','-','+'];

		$('.calc td').on('click', function(e){
			var key = $(this);
			var keyValue = key.text();
			var lastValue;
			
			// Limpa a tela
			if (key.is('.clear')) {
				screenEl.text('0');
				return false;			
			}

			// Calcula expressao
			if (key.is('.result')) {
				result = eval(screenEl.text());
				screenEl.text(result);
				screenEl.addClass('calculated');
				return false;
			}

			// Limpa a tela se a expressao tiver sido calculada
			if (screenEl.is('.calculated')) {
				screenEl.text('0');
				screenEl.removeClass('calculated');
			}

			// Remove o 0 inicial para evitar problemas de calculo
			if (screenEl.text() === '0') {
				screenEl.empty();
			}

			// Converte virgula em ponto
			if (keyValue === ',') {
				keyValue = '.';
			}

			// Obtem o ultimo caracter para verificacoes nas operacoes
			lastValue = screenEl.text().charAt(screenEl.text().length - 1);

			// Evita que o calcule se inicie com operacoes
			if (lastValue === '' && key.is('.operation')) {
				screenEl.text('0');
				alert('Digite um número antes de digitar a operação');
				return false;
			}

			// Evita digitar duas operacoes (simbolos) em sequencia
			if ($.inArray(lastValue, operations) >= 0 && key.is('.operation')) {
				alert('Não digite operações em sequência');
				return false;
			}

			// Adiciona o valor no visor
			screenEl.append(keyValue);

		});



});

})(jQuery);

console.log('Implemente a funcionalidade da Calculadora acima em javascript \n Dica: eval');