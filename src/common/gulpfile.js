/* Gulp Config */

'use strict';

// Load plugins
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var cssnano = require('gulp-cssnano');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var size = require('gulp-size');

// error function for plumber
var onError = function (err) {
	gutil.beep();
	console.log(err);
	this.emit('end');
};

// Browser definitions for autoprefixer
var AUTOPREFIXER_BROWSERS = [
'last 5 versions',
'ie >= 8',
'ios >= 7',
'android >= 4.4',
'bb >= 10'
];

var basePaths = {
	src: './',
	dest: './'
};

var paths = {
	styles: {
		src: basePaths.src + 'scss/',
		dest: basePaths.dest + 'css/'
	}
};

gulp.task('css', function() {
	return gulp.src([
		paths.styles.src + '**/*.scss',
		])
	.pipe(plumber({ errorHandler: onError }))
	.pipe(sass({errLogToConsole: true}))
	.pipe(gulp.dest(paths.styles.dest))
	.pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
	.pipe(rename({ suffix: '.min' }))
	.pipe(cssnano({autoprefixer: false}))
	.pipe(gulp.dest(paths.styles.dest))
	.pipe(size({title: 'css'}))
	.pipe(notify({ message: 'Styles task complete' }));
});

//tasks
gulp.task('default', ['css'], function(){
	gulp.watch(paths.styles.src + '**/*', ['css']);
});